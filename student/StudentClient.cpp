#include <iostream>
#include <string>
#include <sstream>
#include <limits>
#include <unistd.h>

#include "../communication/Communication.h"
#include "../communication/Common.h"
#include "Student.h"

using namespace std;
using namespace common;



//in general, need to add error handling policy
bool initStudentClient(Communication& communication)
{
	bool status = false;
	
	//open socket fd in system
	communication.openSocketFD();
	
	//define socket address
	sockaddr_in serverAddress;
	communication.defineSockaddr(serverAddress, common::SERVER_IP, common::SERVER_PORT);

	//activate the socket - connect the sokectFD to remote address
	int filureCounter = 0;
	while(! status && filureCounter < 5)
	{
		++filureCounter;
		cout << "try to connect to the bank. trying number: " << filureCounter << endl;
		status = communication.connetToRemoteSocket(serverAddress);
		if(! status)
		{
			//sleep for 5 seconds
			usleep(50000);		
		}
	}
	
	return status;
		
}

//-----------------------------------------------
int chooseNextOptions()
{
	int nextStep = 0;
	cout << endl << endl << "---------------------------------------------------------" << endl;
	cout << "pls choose operation from the list, by press the operation number:" << endl;
	cout << "1. print student status" << endl;
	cout << "2. deposit money to bank account"<< endl;
	cout << "3. withdraw money from bank account"<< endl;
	cout << "4. present current bank account balance" << endl;
	cout << "5. get out of here!" << endl << endl;
	
	cout << "your action: ";
	if( ! (cin >> nextStep))
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		nextStep = 0;
	}
	
	cout << endl;
	return nextStep;
}


//----------------------------------------------------------------
//--------------------------------- main -------------------------
//----------------------------------------------------------------

int main()
{
	Communication communication;
	if( ! initStudentClient(communication))
	{
		cout << "failed to create connection to the bank!" << endl;
		return -1;		
	}
	
	//create the student 
	string id;
	cout << "---------------------------------------------------------" << endl;
	cout << "wellcome, to create a student account, pls insert ID and Name" << endl;
	cout << "ID: ";
	cin >> id;
	cout << "the student ID is: " << id << endl;
	
	string name;
	cout << "name: ";
	cin >> name;
	cout << "the student name is: " << name << endl;
	
	Student student(id, name);
	student.getStudentAccountBank(&communication);
	
	//stat perform actions on the student
	bool shouldIContiune = true;
	int nextStep = 0;
	int amount = 0;
	while(shouldIContiune)
	{
		nextStep = chooseNextOptions();
		switch (nextStep)
		{
			case 1:
				cout << "student status: " << endl;
				cout << student.toString();
				break;
				
			case 2:
				cout << "pls set amount to deposit: ";
				cin >> amount;
				cout << student.depositMoney(amount);
				break;
				
			case 3:
				cout << "pls set amount to withdraw: ";
				cin >> amount;
				cout << student.withdrawMoney(amount);
				break;
				
			case 4:
				cout << student.getBalance(amount);
				cout << "current bank account balance: " << amount;
				break;
				
			case 5:
				cout << "pls close the connection to the bank" << endl;
				student.closeConnection();
				shouldIContiune = false;
				break;
				
			default:
				cout << "wrong choice! input must be number between 1 to 5!" << endl;
		}
	}
	
	cout << "goodbay!";
	
	return 0;
}
