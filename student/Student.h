/*
	auther: yair yavneel
	date: 24.8 too:late
	description: this API created to present the student characteristics and available functionality
*/

#ifndef __STUDENT__
#define __STUDENT__

#include <string>
#include "../communication/Communication.h"
#include "../communication/Common.h"

using namespace std;
using namespace common;

class Student
{
	
	public: 
		Student(string id, string fullName);
		~Student();
		string getId();
		string getName();

		//get the bank account connection.
		//to make it simple - the any student have one account (one to one).
		void getStudentAccountBank(Communication* communicationToBankAccount);

		//get the current balance from the bank account.
		//returns: 
		//	- success: true, output argument will populate with the current balance.
		//	- failure: 
		//		- false in case of business logic. like the connection wasnt assign yet
		//		- exception in case of technical issues.
		bool getBalance(int& balance);
		
		//desposit amount to the student bank account.
		//returns: 
		//	- success: true
		//	- failure: 
		//		- false in case of business logic. like the limit of amount that can be desposit at once.
		//		- exception in case of technical issues.
		bool depositMoney(int amount);
		
		//withdrow amount to the student bank account.
		//returns: 
		//	- success: true
		//	- failure: 
		//		- false in case of business logic. like the limit was reached (0 by default)
		//		- false in case of technical issues, exception will be thrown
		bool withdrawMoney(int amount);
		
		//the student will close the connection with the bank account.
		//NOTE!! this action should not delete the communitcation.
		bool closeConnection();
		
		//returns current student status as string
		string toString();
		
	private: 
		bool sendAndReceived(AccountBankMsg& bankAccountMsg);
		void initAccountBankMsg(AccountBankMsg& msg, ActionType actionType, int amount, bool status);
		
		string m_name;
		string m_id;
		Communication* p_communicationToBankAccount;
};


#endif // __STUDENT__
	
