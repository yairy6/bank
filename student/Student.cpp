#include "Student.h"
#include "../communication/Communication.h"
#include "../communication/Common.h"

#include <sstream>
#include <iostream>
	
Student::Student(string id, string name)
	:	m_name(""), 
		m_id(""), 
		p_communicationToBankAccount(NULL)
{
  this->m_id = id;
  this->m_name = name;		
}

Student::~Student()
{
	//do not delete, as the student is not responsible on the account
	p_communicationToBankAccount = NULL;
}

//---------------------------------------------------	
string Student::getId()
{
	return m_id;
}

//---------------------------------------------------	
string Student::getName()
{
	return m_name;
}

//---------------------------------------------------	
void Student::getStudentAccountBank(Communication* communicationToBankAccount)
{
	p_communicationToBankAccount = communicationToBankAccount;
}

//---------------------------------------------------	
bool Student::getBalance(int& balance)
{
	bool status = true;
	AccountBankMsg msg;
	initAccountBankMsg(msg, common::BALANCE, 0, true);

	cout << "getting account balance" << endl;
	status = sendAndReceived(msg);
	//error handling of satus (thechnial) and msg.status (business)
	balance = msg.amount;
	
	cout << "current balance is: " << balance << endl;
	return status;
}

//---------------------------------------------------	
bool Student::depositMoney(int amount)
{
	bool status = true;
	common::AccountBankMsg msg;
	initAccountBankMsg(msg, common::DEPOSIT, amount, true);

	cout << "trying to deposit the amount: " << msg.amount << endl;
	status = sendAndReceived(msg);
	//error handling of satus (thechnial) and msg.status (business)
	cout << "deposit status: " << msg.status << endl;
	
	//error handling
	cout << "balance after deposit: " << msg.amount << endl;
	
	return status;
}

//---------------------------------------------------	
bool Student::withdrawMoney(int amount)
{
	bool status = true;
	common::AccountBankMsg msg;
	initAccountBankMsg(msg, common::WITHDRAW, amount, true);

	cout << "trying to withdraw the amount: " << msg.amount << endl;
	status = sendAndReceived(msg);
	//error handling of satus (thechnial) and msg.status (business)
	cout << "withdraw status: " << msg.status << endl;
	
	//error handling
	cout << "balance after withdraw: " << msg.amount << endl;
	
	return status;
}

//---------------------------------------------------	
bool Student::closeConnection()
{
	bool status = true;
	common::AccountBankMsg msg;
	initAccountBankMsg(msg, common::CLOSE, 0, true);

	cout << "closing the connection with the bank account"<< endl;
	status = sendAndReceived(msg);
	//error handling of satus (thechnial) and msg.status (business)
	cout << "close status: " << msg.status << endl;
	
	p_communicationToBankAccount = NULL;
	return status;		
}


//---------------------------------------------------	
string Student::toString()
{
	//try catch
	stringstream sstr;
	int balance = 0;
	getBalance(balance);
	sstr << "student - id: " << m_id << ", name: " << m_name << ", balance: " << balance;
	return sstr.str();
}

//---------------------------------------------------	
//-------------------------- private ----------------	
//---------------------------------------------------	
bool Student::sendAndReceived(AccountBankMsg& msg)
{
	bool status = true;
	cout << "Student sending msg - actionType: " << msg.actionType << ", amount: " << msg.amount << ", status: " << msg.status << endl;
	status = p_communicationToBankAccount->sendMsg(p_communicationToBankAccount->getSocketFD(), &msg, sizeof(msg));
		
	if(status)
	{
		cout << "Student wait for response" << endl;
		p_communicationToBankAccount->receiveMsg(p_communicationToBankAccount->getSocketFD(), &msg, sizeof(msg));
		cout << "Student reveiced msg - actionType: " << msg.actionType << ", amount: " << msg.amount << ", status: " << msg.status << endl;	
	}
	else
	{
		cout << "falied to connect with the bank" << endl;	
	}
	
	return status;
}
	
//---------------------------------------------------	
void Student::initAccountBankMsg(AccountBankMsg& msg, ActionType actionType, int amount, bool status)
{
	msg.actionType = actionType;
	msg.amount = amount;
	msg.status = status;
}
