/*
	auther: yair yavneel
	date: 24.8 too:late
	description: this API created to present a bank account characteristics and available functionality
*/

#ifndef __BANK_ACCOUNT__
#define __BANK_ACCOUNT__

#include <string>

using namespace std;

class BankAccount
{
	public: 
		BankAccount(string id);
		string getId();
		
		//get the current balance from the bank account.
		//returns: the current balance.
		int getBalance();
		
		//desposit amount to the bank account, and update the balance according the changes
		//returns: 
		//	- success: true
		//	- failure: 
		//		- false in case of business logic. like the limit of amount that can be desposit at once.
		//		- false in case of technical issues,exception will be thrown
		bool depositMoney(int amount);
		
		//withdrow amount to the bank account, and update the balance according the changes
		//returns: 
		//	- success: true
		//	- failure: 
		//		- false in case of business logic. like the limit was reached (0 by default)
		//		- false in case of technical issues,exception will be thrown
		bool withdrawMoney(int amount);
		
		//returns current account status as string
		string toString();
		
	private: 
		string m_id;
		int m_balance;
};
#endif // __BANK_ACCOUNT__
