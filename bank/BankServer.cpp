#include <iostream>
#include <string>
#include <sstream>
#include "../communication/Communication.h"
#include "../communication/Common.h"
#include "BankAccount.h"



using namespace std;



//need to add error handling 
void initBankServer(Communication& communication)
{
	//open socket fd in system
	communication.openSocketFD();
	
	//define socket address
	sockaddr_in serverAddress;
	communication.defineSockaddr(serverAddress, common::SERVER_IP, common::SERVER_PORT);

	//activate the socket - bind the socket to address, and listen to the socket.
	communication.bindAndListenToSocket(serverAddress);	
}


//----------------------------------------------------------------
//--------------------------------- main -------------------------
//----------------------------------------------------------------

int main()
{
	Communication communication;
	initBankServer(communication);

	//wait for the first client
	sockaddr_in dummy;
	int socketFDThatContactMe = 0;
	cout << "bank server waiting to accept msg, and make connection with client" << endl;
	communication.acceptConnectionFromRemoteSocket(dummy, socketFDThatContactMe);
	
	cout << "creating bankAccount";
	BankAccount bankAccount("myBankAccount123");
	
	//start the ping-pong with client
	common::AccountBankMsg msg;
	bool shouldIContiune = true;
	while(shouldIContiune)
	{
		cout << endl<< "-----------------------" << endl;
			
		cout << "bank receiving msg" << endl;
		communication.receiveMsg(socketFDThatContactMe, &msg, sizeof(msg));
		cout << "the received msg is - actionType: " << msg.actionType << ", amount: " << msg.amount << endl;
		switch(msg.actionType)
		{
			case common::BALANCE:
				cout << "current bank account balance: " << bankAccount.getBalance();
				msg.amount = bankAccount.getBalance();
				msg.status = true;
				break;
			
			case common::DEPOSIT:
				cout << "amount to deposit: " << msg.amount ;
				bankAccount.depositMoney(msg.amount);
				msg.amount = bankAccount.getBalance();
				msg.status = true;
				break;
				
			case common::WITHDRAW:
				cout << "amount to withdraw: " << msg.amount << endl;
				msg.status = bankAccount.withdrawMoney(msg.amount);
				msg.amount = bankAccount.getBalance();	
				break;
				
			case common::CLOSE:				
				cout << "we done here. pls exit" << endl;
				shouldIContiune = false;
				break;
				
			default:
				cout << "unknown msg type" << endl;
		}
		
		cout << "server sending answer" << endl;
		communication.sendMsg(socketFDThatContactMe, &msg, sizeof(msg));
	}
	
	cout << "server closing accepted socked fd" << endl;
	communication.closeSocket(socketFDThatContactMe);
	cout<< "goodbay!";
	return 0;
}
