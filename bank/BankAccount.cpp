#include "BankAccount.h"

#include <string>
#include <iostream>
#include <sstream>

BankAccount::BankAccount(string id)
	: m_id(""),
		m_balance(0)
{
	this->m_id = id;		
}

//---------------------------------------------------	
string BankAccount::getId()
{
	return m_id;
}

//---------------------------------------------------	
int BankAccount::getBalance()
{
	cout << "current balace: " << m_balance << endl;
	return m_balance;
}

//---------------------------------------------------	
bool BankAccount::depositMoney(int amount)
{
	cout << "current balace: " << m_balance << ", before deposit: " << amount << endl;
	m_balance += amount;
	cout << "new balace: " << m_balance << endl;
	return true;
}

//---------------------------------------------------	
bool BankAccount::withdrawMoney(int amount)
{
	bool status = true;
	if(m_balance - amount < 0)
	{
		cout << "there is no enough money to withdraw: " << amount << ", as balance is: " << m_balance << endl;
		status = false;
	}
	else
	{
		m_balance -= amount;
		cout << "new balace: " << m_balance << endl;

	}
	return status;
}

//---------------------------------------------------	
string BankAccount::toString()
{
	stringstream sstr;
	sstr << "for account id: " << m_id <<", current balace is: " << m_balance << endl;
	return sstr.str();
}
