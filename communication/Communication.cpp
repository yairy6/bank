#include "Communication.h" 

#include <iostream>		
#include <stdio.h>			/* perror */
#include <unistd.h>			/* close() */
#include <string>			/* strlen() memset() */
#include <cstring>
#include <sys/types.h>		/* size_t ssize_t */
#include <sys/socket.h>		/* send listen */
#include <arpa/inet.h>		/* pton*/
#include <netinet/in.h>		/* sockadrr_in */

#include "Common.h"

using namespace std;

bool Communication::openSocketFD()
{
	bool status = true;
	m_listeningSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (m_listeningSocket == -1)
	{
		perror("serverSocket");
		status = false;
	}	
	
	return status;
}

//-------------------------------------------------------
int Communication::getSocketFD()
{
	return m_listeningSocket;
}

//-------------------------------------------------------
bool Communication::defineSockaddr(sockaddr_in& i_sockaddr, string ipv4, int port)
{
	bool status = true;
	
	/* init the sockaddr_in for bind to the serverSocket */
	memset(&i_sockaddr, 0 , sizeof(i_sockaddr));
	i_sockaddr.sin_family = AF_INET;
	
	/* htons - convert the host port(short number) endian to network endian
	 (be sure that the endian case will not warp the port number) */
	i_sockaddr.sin_port = htons(port);
	
	/* convert the SERVER_IP from char* to network IP */
	int checking = inet_pton(AF_INET, ipv4.c_str(), &i_sockaddr.sin_addr);
	if (checking == 0)
	{
		puts("pton: string is missing");
	}
	else if (checking == -1)
	{
		perror("pton");
		status = false;
	}	

	return status;
}

//-------------------------------------------------------
bool Communication::connetToRemoteSocket(sockaddr_in& i_sockaddrToConnect)
{
	bool status = true;
	int checking = connect(m_listeningSocket, (sockaddr*)&i_sockaddrToConnect, sizeof(i_sockaddrToConnect));
	if (checking == -1)
	{
		perror("connect");
		status = false;
	}
	return status;
}

//-------------------------------------------------------
bool Communication::bindAndListenToSocket(sockaddr_in& i_sockaddr)
{
	/* bind to the server socket, and define listen queue (tcp) for packets */	
	int optval = 1;
	int checking = setsockopt(m_listeningSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	if(checking == -1)
	{
		perror("setsockopt");
		return false;	
	}
	
	checking = bind(m_listeningSocket, (sockaddr*)&i_sockaddr, sizeof(i_sockaddr));
	if(checking == -1)
	{
		perror("bind");
		return false;
	}

	checking = listen(m_listeningSocket, 5);
	if(checking == -1)
	{
		perror("listen");
		return false;		
	}

	return true;
}

//-------------------------------------------------------
bool Communication::acceptConnectionFromRemoteSocket(sockaddr_in& o_acceptedSockaddr, int& o_acceptedSocket)
{
	bool status = true;
	
	socklen_t sockaddrlen = sizeof(o_acceptedSockaddr);
	o_acceptedSocket = accept(m_listeningSocket, (sockaddr*)&o_acceptedSockaddr, &sockaddrlen);
	if (o_acceptedSocket == -1)
	{
		perror("accept");
		status = false;
	}
	
	return status;
}

//-------------------------------------------------------
bool Communication::sendMsg(int socketToSend, void* msg, size_t msgLen)
{
	bool status = true;

	/*send msg to server */
	ssize_t nBytes = send(socketToSend, msg, msgLen, 0);
	if(nBytes == -1)
	{
		perror("send");
		status = false;
	}
	else if( ! nBytes)
	{
		perror("send");
		status = false;
	}
	else if(nBytes < msgLen)
	{
		perror("send");
		status = false;
	}
	
	return status;
}
	
//-------------------------------------------------------
bool Communication::receiveMsg(int i_socketToGetMsgFrom, void* o_buffer, size_t i_bufSize)
{
	bool status = true;
	ssize_t	nBytes = recv(i_socketToGetMsgFrom, o_buffer, i_bufSize, 0);
	if(nBytes == -1)
	{
		perror("recv");
		status = false;
	}
	
	return status;
}

//-------------------------------------------------------
bool Communication::closeSocket(int socket_fd)
{
	close(socket_fd);
}

//-------------------------------------------------------
Communication::~Communication()
{
	close(m_listeningSocket);
}


