/*
	auther: yair yavneel
	date: 26.8 too:late
	description: this API created for communication infrastructure abilities.
		this communication based on IPv4 and TCP protocol.
*/


#ifndef __COMMUNICATION__ 
#define __COMMUNICATION__

#include <string>
#include <netinet/in.h>

using namespace std;

class Communication
{
	public:
		// close the socket FD
		~Communication();
		
		//open a socket FD
		bool openSocketFD();
		int getSocketFD();
		
		//by given host and port, the address structure will be define.
		bool defineSockaddr(sockaddr_in& i_sockaddr, string ipv4, int port);
		
		//open the ability to connect with remote server. 
		bool connetToRemoteSocket(sockaddr_in& i_sockaddrToConnect);
		
		//this bind the socket address, and define the listen queue
		bool bindAndListenToSocket(sockaddr_in& i_sockaddr);
		
		//witing to the client to accept the clinet connction details
		bool acceptConnectionFromRemoteSocket(sockaddr_in& o_acceptedSockaddr, int& o_acceptedSocket);
		
		bool sendMsg(int socketToSend, void* msg, size_t msgLen);
		bool receiveMsg(int i_socketToGetMsgFrom, void* o_buffer, size_t i_bufSize);
		bool closeSocket(int socket_fd);
		
	private:
		int m_listeningSocket;
};


#endif //__COMMUNICATION__