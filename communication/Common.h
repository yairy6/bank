/**************************************************************************************
    Author :	Yair Yavneel
    Creation date :     *.*.14
    Date last modified :  *.*.14
    Description : 	
***************************************************************************************/
#ifndef __COMMON__
#define __COMMON__

#include<string>

using namespace std;

namespace common
{
	static const string SERVER_IP = "127.0.0.1";
	static const int SERVER_PORT = 5060;
	
	enum ActionType
	{
		BALANCE = 0,
		DEPOSIT = 1,
		WITHDRAW = 2,
		CLOSE = 3
	};
	
	struct AccountBankMsg
	{
		ActionType actionType;
		int amount;
		bool status;
	};
}

#endif // __COMMON__

